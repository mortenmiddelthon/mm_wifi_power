// Setup file. Alter file to your local WLAn setup and rename to "setup.h"

#define WLAN_SSID       "SSID"           // cannot be longer than 32 characters!
#define WLAN_PASS       "PASSWORD"
// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2

// What page to grab!
#define WEBSITE      "10.0.0.40"
#define WEBPAGE      "/power/?"
