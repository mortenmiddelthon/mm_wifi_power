#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>

// Include the setup file
#include "setup.h"

// Status LED
#define LED 9

// Wifi breakout setup
#define ADAFRUIT_CC3000_IRQ 3
#define ADAFRUIT_CC3000_VBAT 5
#define ADAFRUIT_CC3000_CS 10
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT, SPI_CLOCK_DIVIDER);

// Timeout value in ms for reading from web server
#define IDLE_TIMEOUT_MS	200

uint32_t ip;

// Set the analog input pin where the photo resistor is connected
#define PHOTO 0

uint32_t timestamp;
uint32_t interval = 60000;
uint16_t counter = 0;
uint8_t c = 0;

// The light_state variable determines when the photo resistor has detected a pulse
// This is to avoid counting one pulse more than once
uint8_t light_state = 0;

// Set the photo resistor light threshold. This may have to be adjusted to your 
// environment
uint16_t threshold = 180;

void setup(void) {
	Serial.begin(9600);
	Serial.println("[Get power usage]");
	pinMode(LED, OUTPUT);

	/* Initialise the module */
	Serial.println(F("\nInitializing..."));
	if (!cc3000.begin())
	{
		Serial.println(F("Couldn't begin()! Check your wiring?"));
		while(1) {
			digitalWrite(LED, HIGH);
			delay(500);
			digitalWrite(LED, LOW);
			delay(500);
		}
	}
	
	Serial.print(F("\nAttempting to connect to ")); Serial.println(WLAN_SSID);
	if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
		Serial.println(F("Failed!"));
		while(1) {
			digitalWrite(LED, HIGH);
			delay(500);
			digitalWrite(LED, LOW);
			delay(500);
		}
	}
	 
	Serial.println(F("Connected!"));
	
	/* Wait for DHCP to complete */
	Serial.println(F("Request DHCP"));
	while (!cc3000.checkDHCP())
	{
		delay(100); // ToDo: Insert a DHCP timeout!
	}	

	/* Display the IP address DNS, Gateway, etc. */	
	while (! displayConnectionDetails()) {
		delay(1000);
	}

	ip = WEBSITE;
	/*
	// Try looking up the website's IP address
	Serial.print(WEBSITE); Serial.print(F(" -> "));
	while (ip == 0) {
		if (! cc3000.getHostByName(WEBSITE, &ip)) {
			Serial.println(F("Couldn't resolve!"));
		}
		delay(500);
	}
	*/

	cc3000.printIPdotsRev(ip);
	Serial.println();
	digitalWrite(LED, HIGH);

	// Set the timestamp to current running time
	timestamp = millis();
	light_state = 0;
}

void loop(void) {
	uint16_t light = analogRead(PHOTO);
	// If time interval has been reached, print out the current pulse count
	if(millis() > timestamp + interval) {
		timestamp = millis();
		Serial.println();
		Serial.print("#");
		Serial.print(c);
		Serial.print(" Blinks: ");
		Serial.println(counter);

		// Post pulse count
		digitalWrite(LED, LOW);
		postUsage(counter, c);
		digitalWrite(LED, HIGH);

		counter = 0;
		if(c >= 255) {
			c = 0;
		}
		else {
			c++;
		}
	}
	// Count the pulse if light as above threshold value
	if(light > threshold && light_state == 0) {
		light_state = 1;
		counter++;
		Serial.println(light);
	}
	else if(light < threshold) {
		light_state = 0;
	}
	delay(5);
}

/**************************************************************************/
/*!
		@brief	Tries to read the IP address and other connection details
*/
/**************************************************************************/
bool displayConnectionDetails(void)
{
	uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;
	
	if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
	{
		Serial.println(F("Unable to retrieve the IP Address!\r\n"));
		return false;
	}
	else
	{
		Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
		Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
		Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
		Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
		Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
		Serial.println();
		return true;
	}
}

void postUsage (uint8_t pulses, uint8_t counter) {
	char getString[40];
	Serial.println("Posting...");
	sprintf(getString, "GET /kwh/%i/%i HTTP/1.0\r\n", pulses, counter);
	Serial.print("getString: ");
	Serial.println(getString);
	Adafruit_CC3000_Client www = cc3000.connectTCP(ip, 80);
	// char URL[] = WEBPAGE;
	// sprintf(getString, "GET %spulses=%i&counter=%i HTTP/1.1\r\n", URL, pulses, counter);

	if (www.connected()) {
		delay(50);
		www.println(getString);
	} else {
		Serial.println(F("Connection failed"));		
	}
	Serial.println(F("-------------------------------------"));
  
	unsigned long lastRead = millis();
	while (www.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS)) {
		while (www.available()) {
			char c = www.read();
			Serial.print(c);
			lastRead = millis();
		}
	}
	www.close();
}
