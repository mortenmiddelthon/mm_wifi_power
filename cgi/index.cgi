#!/usr/bin/perl

use warnings;
use strict;
use CGI;
my $q = CGI->new;

print "Content-Type: text/plain\n\n";
print "Hello world!\n";

my @params = $q->param();
foreach my $p (@params) {
	print "$p: ";
	print $q->param($p);
	print "\n";
}

